import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'Jefferson',
		lastname: "Alvarez"
	}
});

export default app;